# develloper-challenge
```sh
> http://localhost:8080/actuator/health
```
```sh
> http://localhost:8080/swagger-ui.html
```
```sh
> POST http://localhost:8080/v1/api/processing
```
```sh
> GET http://localhost:8080/v1/api/management
```
```sh
> GET http://localhost:8080/v1/api/analytical  (?cpf=&value=&expirationDate=)
```

## Exemplo de uso

Baixe o projeto e execute os passos abaixo:

* 1
    * Execute o comando maven no diretorio raiz do projeto

```sh
mvn clean instal
```

* 2
    * execute o comando abaixo no diretorio target do projeto'

```sh
java -jar developer-challenge-0.0.1.jar
```

* 3
    * o serviço REST será exposto na porta 8080

```sh
http://localhost:8080/swagger-ui.html
```

