package com.desafio.developerchallenge.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.desafio.developerchallenge.exception.UploadFileException;
import com.desafio.developerchallenge.model.ManagementResponse;
import com.desafio.developerchallenge.model.UploadZipFileResponse;
import com.desafio.developerchallenge.service.impl.FileProcessService;

import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;

/**
 * @author HMOURA
 *
 */

@RestController
@RequestMapping(value = "/v1/api")
@Log4j2
public class UploadZipController {

	@Autowired
	private FileProcessService uploadFileService;

	@PostMapping(value = "/processing", produces = APPLICATION_JSON_UTF8_VALUE)
	@ApiOperation(value = "receives a zip file containing the mailing file and billing file ", response = UploadZipFileResponse.class)
	public ResponseEntity<?> processFile(@RequestParam("file") MultipartFile file) {
		try {
			log.info("[processFile] starting processing: " + file.getOriginalFilename());
			return new ResponseEntity<>(this.uploadFileService.processZipFile(file), HttpStatus.CREATED);

		} catch (UploadFileException e) {
			log.error(e.getMessage(), e);
			throw e;

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(value = "/management", produces = APPLICATION_JSON_UTF8_VALUE)
	@ApiOperation(value = "returns all files data ", response = ManagementResponse[].class)
	public ResponseEntity<?> management() {
		return new ResponseEntity<>(this.uploadFileService.findAllManagement(), HttpStatus.ACCEPTED);
	}

	@GetMapping(value = "/analytical", produces = APPLICATION_JSON_UTF8_VALUE)
	@ApiOperation(value = "returns processed files", response = ManagementResponse[].class)
	public ResponseEntity<?> analytical(@RequestParam(required = false) String cpf,
			@RequestParam(required = false) String value, @RequestParam(required = false) String expirationDate) {
		log.info("(analytical) cpf           : " + cpf);
		log.info("(analytical) value         : " + value);
		log.info("(analytical) expirationDate: " + expirationDate);

		return new ResponseEntity<>(this.uploadFileService.findAllAnalytical(cpf, value, expirationDate), HttpStatus.ACCEPTED);
	}

}
