package com.desafio.developerchallenge.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author HMOURA
 *
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UploadFileException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public UploadFileException(String message) {
		super(message);
	}
	
	public UploadFileException(String message, Throwable cause) {
		super(message, cause);
	}

}
