package com.desafio.developerchallenge.model;

import lombok.Data;

@Data
public class AnalyticalResponse {

	private String cpf;
	private String name;
	private String value;
	private String expirationDate;
	private String address;
	private int age;
	private String fileName;
	private int processed;
	private String status;
	private String protocol;
	
	public AnalyticalResponse(String cpf, String name, String value, String expirationDate, String address, int age,
			String fileName, int processed, String status, String protocol) {
		super();
		this.cpf = cpf;
		this.name = name;
		this.value = value;
		this.expirationDate = expirationDate;
		this.address = address;
		this.age = age;
		this.fileName = fileName;
		this.processed = processed;
		this.status = status;
		this.protocol = protocol;
	}

	public AnalyticalResponse() {
		super();
	}
}
