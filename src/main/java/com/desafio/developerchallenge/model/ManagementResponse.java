package com.desafio.developerchallenge.model;

import java.util.Date;

import lombok.Data;

@Data
public class ManagementResponse {

	private String fileName;
	private String protocol;
	private Date receiptDate;
	private Date processingStart;
	private Date processingEnd;
	private String status;
	private Integer numberObjects;
	private Integer processed;
	private Integer invalidRegisters;
	private Integer noMatches;
	private Integer matches;

	public ManagementResponse(String fileName, String protocol, Date receiptDate, Date processingStart,
			Date processingEnd, String status, Integer numberObjects, Integer processed, Integer invalidRegisters,
			Integer noMatches, Integer matches) {
		super();
		this.fileName = fileName;
		this.protocol = protocol;
		this.receiptDate = receiptDate;
		this.processingStart = processingStart;
		this.processingEnd = processingEnd;
		this.status = status;
		this.numberObjects = numberObjects;
		this.processed = processed;
		this.invalidRegisters = invalidRegisters;
		this.noMatches = noMatches;
		this.matches = matches;
	}

	public ManagementResponse() {
		super();
	}

}
