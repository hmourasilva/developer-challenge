package com.desafio.developerchallenge.model;

import java.util.Date;

import lombok.Data;

/**
 * @author HMOURA
 *
 */

@Data
public class UploadZipFileResponse {

	private String msg;
	private String protocol;
	private String fileZipName;
	private String fileType;
	private long size;
	private Date receiptDate;

	public UploadZipFileResponse() {
		super();
	}

	public UploadZipFileResponse(String msg, String protocol, String fileZipName, String fileType, long size,
			Date receiptDate) {
		super();
		this.msg = msg;
		this.protocol = protocol;
		this.fileZipName = fileZipName;
		this.fileType = fileType;
		this.size = size;
		this.receiptDate = receiptDate;
	}

}
