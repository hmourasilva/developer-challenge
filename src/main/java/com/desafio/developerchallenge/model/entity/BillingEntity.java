package com.desafio.developerchallenge.model.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Data;

@Entity
@Table(name = "BILLING")
@Data
@JsonIdentityInfo(
        generator = ObjectIdGenerators.StringIdGenerator.class,
        property="id")
public class BillingEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="BILLING_VALUE")
	private BigDecimal value;
	
	@Column(name="BILLING_CPF")
	private String cpf;
	
	@Column(name="BILLING_INVOICE_DUE")
	@Temporal(TemporalType.DATE)
	private Date invoiceDue;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "mailing_id", insertable=true, updatable = true, nullable = false)
	private MailingEntity mailing;

}
