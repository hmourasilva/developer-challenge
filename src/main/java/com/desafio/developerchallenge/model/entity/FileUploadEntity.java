package com.desafio.developerchallenge.model.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Data;

@Entity
@Table(name = "ZIPFILE")
@Data
@JsonIdentityInfo(
        generator = ObjectIdGenerators.StringIdGenerator.class,
        property="id")
public class FileUploadEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="ZIPFILE_FILE_NAME")
	private String fileName;
	
	@Column(name="ZIPFILE_PROTOCOL")
	private String protocol;
	
	@Column(name="ZIPFILE_RECEIPT_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date receiptDate;
	
	@Column(name="ZIPFILE_PROCESSING_START")
	@Temporal(TemporalType.TIMESTAMP)
	private Date processingStart;
	
	@Column(name="ZIPFILE_PROCESSING_END")
	@Temporal(TemporalType.TIMESTAMP)
	private Date processingEnd;
	
	@Column(name="ZIPFILE_STATUS")
	private String status;
	
	@Column(name="ZIPFILE_NUMBER_OBJECTS")
	private Integer numberObjects;
	
	@Column(name="ZIPFILE_PROCESSED")
	private Integer processed;
	
	@Column(name="ZIPFILE_INVALID_REGISTERS")
	private Integer invalidRegisters;
	
	@Column(name="ZIPFILE_NOMATCHES")
	private Integer noMatches;
	
	@Column(name="ZIPFILE_MATCHES")
	private Integer matches;
			
	@OneToMany(mappedBy="fileUpload", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<MailingEntity> mailing;
	
	public List<MailingEntity> getMailing(){
		if(mailing == null) {
			mailing = new ArrayList<MailingEntity>();
		} 
		return mailing;
	}
}
