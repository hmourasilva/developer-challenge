package com.desafio.developerchallenge.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Data;

@Entity
@Table(name = "MAILING")
@Data
@JsonIdentityInfo(
        generator = ObjectIdGenerators.StringIdGenerator.class,
        property="id")
public class MailingEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="MAILING_CPF")
	private String cpf;
	
	@Column(name="MAILING_NAME")
	private String name;
	
	@Column(name="MAILING_ADDRESS")
	private String address;
	
	@Column(name="MAILING_AGE")
	private Integer age;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "fileUpload_id", insertable=true, updatable = true)
	private FileUploadEntity fileUpload;
	
	@OneToMany(mappedBy="mailing", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<BillingEntity> billing;
	
	public List<BillingEntity> getBilling(){
		if(billing == null) {
			billing = new ArrayList<BillingEntity>();
		} 
		return billing;
		
	}
}
