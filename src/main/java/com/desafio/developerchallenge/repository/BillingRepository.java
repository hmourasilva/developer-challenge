package com.desafio.developerchallenge.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.desafio.developerchallenge.model.entity.BillingEntity;

@Repository
public interface BillingRepository extends JpaRepository<BillingEntity, Long> {
	
	public List<BillingEntity> findByCpf(String cpf);

	public List<BillingEntity> findByValue(BigDecimal value);
	
	public List<BillingEntity> findByInvoiceDue(Date invoiceDue);
	
	public List<BillingEntity> findByCpfAndValueAndInvoiceDue(String cpf, BigDecimal value, Date invoiceDue);
}
