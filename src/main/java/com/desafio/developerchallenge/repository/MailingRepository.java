package com.desafio.developerchallenge.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.desafio.developerchallenge.model.entity.MailingEntity;

@Repository
public interface MailingRepository extends JpaRepository<MailingEntity, Long> {

	public MailingEntity findByCpf(String cpf);
	
	@Query(value = "SELECT m FROM MailingEntity m WHERE m.cpf = :cpf AND m.fileUpload.protocol = :protocol")
	public MailingEntity findByCpfProtocol(String cpf, String protocol);
}
