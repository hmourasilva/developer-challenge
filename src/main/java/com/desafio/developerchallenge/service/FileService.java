package com.desafio.developerchallenge.service;

import java.util.List;

public interface FileService {

	public Object load(List<String> line) throws Exception;
}
