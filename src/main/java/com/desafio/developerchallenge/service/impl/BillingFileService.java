package com.desafio.developerchallenge.service.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.stereotype.Service;

import com.desafio.developerchallenge.model.entity.BillingEntity;
import com.desafio.developerchallenge.service.FileService;
import com.desafio.developerchallenge.util.CpfUtils;

@Service
public class BillingFileService implements FileService {

	public final static String FILE_POSFIX = "billing.csv";
	
	private CpfUtils cpfService = new CpfUtils();
	
	@Override
	public BillingEntity load(List<String> line) throws Exception {
		if(line.size() < 3) {
			throw new Exception("invalid line");
		}
		
		BillingEntity billing = new BillingEntity();
		billing.setCpf(this.cpfService.isValid(line.get(0)));
		billing.setValue(new BigDecimal(line.get(1).replaceAll(",", ".")));
		billing.setInvoiceDue(new SimpleDateFormat("dd/MM/yyyy").parse(line.get(2)));
		
		return billing;
	}

}
