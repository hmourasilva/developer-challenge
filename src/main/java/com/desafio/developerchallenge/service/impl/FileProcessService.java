package com.desafio.developerchallenge.service.impl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.desafio.developerchallenge.exception.UploadFileException;
import com.desafio.developerchallenge.model.AnalyticalResponse;
import com.desafio.developerchallenge.model.ManagementResponse;
import com.desafio.developerchallenge.model.UploadZipFileResponse;
import com.desafio.developerchallenge.model.entity.BillingEntity;
import com.desafio.developerchallenge.model.entity.FileUploadEntity;
import com.desafio.developerchallenge.model.entity.MailingEntity;
import com.desafio.developerchallenge.repository.BillingRepository;
import com.desafio.developerchallenge.repository.FileUploadRepository;
import com.desafio.developerchallenge.repository.MailingRepository;
import com.desafio.developerchallenge.service.FileService;
import com.desafio.developerchallenge.zip.FileUtils;

import lombok.extern.log4j.Log4j2;

/**
 * @author HMOURA
 *
 */

@Service
@Log4j2
public class FileProcessService {

	@Autowired
	private FileUtils fileUtil;

	@Autowired
	private FileUploadRepository repository;

	@Autowired
	private MailingRepository mailingRepository;

	@Autowired
	private BillingRepository billingRepository;

	/**
	 * @param file
	 * @return
	 * @throws UploadFileException
	 */
	public UploadZipFileResponse processZipFile(MultipartFile file) throws UploadFileException {
		String receivingProtocol = UUID.randomUUID().toString();
		log.info("(processZipFile) ---> receivingProtocol: " + receivingProtocol);
		return this.storeZipFile(receivingProtocol, file, this.fileUtil.unzipFile(file, receivingProtocol));
	}

	public UploadZipFileResponse storeZipFile(String receivingProtocol, MultipartFile file, List<File> csvFiles) {
		FileUploadEntity entity = new FileUploadEntity();
		entity.setFileName(file.getOriginalFilename());
		entity.setProtocol(receivingProtocol);
		entity.setReceiptDate(new Date());
		entity.setStatus("NP");
		entity = this.repository.save(entity);

		return new UploadZipFileResponse("Received", receivingProtocol, file.getOriginalFilename(),
				file.getContentType(), file.getSize(), entity.getReceiptDate());
	}

	public FileService processFactory(String fileName) {
		log.info("(processFactory) fileName: " + fileName);
		if (fileName.contains("mailing") || fileName.contains("maling")) {
			return new MalingFileService();

		} else {
			return new BillingFileService();
		}
	}

	public List<FileUploadEntity> findUnprocessed() {
		return repository.findByStatus("NP");
	}

	public List<FileUploadEntity> findAll() {
		return this.repository.findAll();
	}

	public List<ManagementResponse> findAllManagement() {
		List<ManagementResponse> managementList = new ArrayList<>();
		try {
			List<FileUploadEntity> files = this.repository.findAll();
			for (FileUploadEntity fileUploadEntity : files) {
				managementList.add(new ManagementResponse(fileUploadEntity.getFileName(),
						fileUploadEntity.getProtocol(), fileUploadEntity.getReceiptDate(),
						fileUploadEntity.getProcessingStart(), fileUploadEntity.getProcessingEnd(),
						fileUploadEntity.getStatus(), fileUploadEntity.getNumberObjects(),
						fileUploadEntity.getProcessed(), fileUploadEntity.getInvalidRegisters(),
						fileUploadEntity.getNoMatches(), fileUploadEntity.getMatches()));
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		return managementList;
	}

	public List<AnalyticalResponse> findAllAnalytical(String cpf, String value, String expirationDate) {
		List<AnalyticalResponse> analyticalList = new ArrayList<>();
		try {
			List<BillingEntity> billingList = this.billingRepository.findByCpf(cpf);
			for (BillingEntity billingEntity : billingList) {
				analyticalList.add(new AnalyticalResponse(billingEntity.getCpf(), billingEntity.getMailing().getName(),
						billingEntity.getValue().toString(),
						new SimpleDateFormat("dd/MM/yyyy").format(billingEntity.getInvoiceDue()),
						billingEntity.getMailing().getAddress(), billingEntity.getMailing().getAge(),
						billingEntity.getMailing().getFileUpload().getFileName(),
						billingEntity.getMailing().getFileUpload().getProcessed(), "Match",
						billingEntity.getMailing().getFileUpload().getProtocol()));
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return analyticalList;
	}

	public FileUploadEntity save(FileUploadEntity entity) {
		return this.repository.saveAndFlush(entity);
	}

	public void saveMailing(MailingEntity entity) {
		this.mailingRepository.saveAndFlush(entity);
	}

	public MailingEntity findMailingByCPF(String cpf) {
		return this.mailingRepository.findByCpf(cpf);
	}

	public MailingEntity findMailingByCPFProtocol(String cpf, String protocol) {
		return this.mailingRepository.findByCpfProtocol(cpf, protocol);
	}

	public BillingEntity saveBilling(BillingEntity entity) {
		return this.billingRepository.saveAndFlush(entity);
	}
}
