package com.desafio.developerchallenge.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.desafio.developerchallenge.model.entity.MailingEntity;
import com.desafio.developerchallenge.service.FileService;
import com.desafio.developerchallenge.util.CpfUtils;

@Service
public class MalingFileService implements FileService {
	
	public final static String FILE_POSFIX = "mailing.csv";
	
	private CpfUtils cpfService = new CpfUtils();

	@Override
	public MailingEntity load(List<String> line) throws Exception {
		if(line.size() < 4) {
			throw new Exception("invalid line");
		}
		
		MailingEntity mailing = new MailingEntity();
		mailing.setName(line.get(0));
		mailing.setAddress(line.get(1));
		mailing.setAge(Integer.parseInt(line.get(2)));
		mailing.setCpf(this.cpfService.isValid(line.get(3)));
		
		return mailing;
	}
}
