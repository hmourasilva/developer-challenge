package com.desafio.developerchallenge.task;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.desafio.developerchallenge.model.entity.BillingEntity;
import com.desafio.developerchallenge.model.entity.FileUploadEntity;
import com.desafio.developerchallenge.model.entity.MailingEntity;
import com.desafio.developerchallenge.service.impl.FileProcessService;
import com.desafio.developerchallenge.zip.FileUtils;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class ProcessFileScheculedTask {

	@Autowired
	private FileProcessService uploadFileService;

	@Autowired
	private FileUtils zipService;
	
	private int processed = 0;
	private int invalidRegisters = 0;
	private int matches = 0;
	private int noMatches = 0;
	private int totalObjects = 0;
	
	private HashMap<String, MailingEntity> data;

	@Scheduled(fixedDelay = (30 * 1000))
	public void process() {
		log.info("##############################################");
		log.info("          START PROCESS FILES");
		log.info("##############################################");

		List<FileUploadEntity> files = this.uploadFileService.findUnprocessed();
		log.info("amount of files to process: " + files.size());

		for (FileUploadEntity fileUploadEntity : files) {
			this.processFile(fileUploadEntity);
		}
	}

	private void processFile(FileUploadEntity fileUploadEntity) {
		fileUploadEntity.setProcessingStart(new Date());

		log.info("MOVING FILES TO PROCESSING ...");
		this.zipService.moveFilesProcessing(fileUploadEntity.getProtocol());

		File mailingFile = this.zipService.getMailingFile(fileUploadEntity.getProtocol());
		log.info("MAILING TO PROCESS: " + mailingFile.getName());
		this.processMaling(mailingFile, fileUploadEntity.getProtocol());

		File billingFile = this.zipService.getBillingFile(fileUploadEntity.getProtocol());
		log.info("BILLING TO PROCESS: " + billingFile.getName());
		this.processBilling(billingFile, fileUploadEntity.getProtocol());
		
		fileUploadEntity.setInvalidRegisters(invalidRegisters);
		fileUploadEntity.setProcessed(processed);
		fileUploadEntity.setMatches(matches);
		fileUploadEntity.setNoMatches(noMatches);
		fileUploadEntity.setNumberObjects(totalObjects);
		
		this.saveProcess(fileUploadEntity);
		this.zipService.moveFilesBackup(fileUploadEntity.getProtocol());
	}

	private void processMaling(File mailingFile, String protocol) {

		data = new HashMap<String, MailingEntity>();

		List<List<String>> csv = this.zipService.readCSV(mailingFile);
		for (List<String> list : csv) {
			try {
				MailingEntity mailing = (MailingEntity) this.uploadFileService.processFactory(mailingFile.getName())
						.load(list);
				data.put(mailing.getCpf(), mailing);
				++processed;
			} catch (Exception e) {
				log.error("(processMaling) INVALID !"+ e.getMessage());
				++invalidRegisters;
			}
		}
		
		this.totalObjects = csv.size();
	}

	private void processBilling(File billingFile, String protocol) {

		List<List<String>> csv = this.zipService.readCSV(billingFile);
		for (List<String> list : csv) {
			try {
				BillingEntity billing = (BillingEntity) this.uploadFileService.processFactory(billingFile.getName())
						.load(list);
				++matches;
				
				MailingEntity mailing = data.get(billing.getCpf());
				if(mailing != null ) {
					billing.setMailing(mailing);
					mailing.getBilling().add(billing);
				}				
				
			} catch (Exception e) {
				log.error("(processBilling) INVALID !"+ e.getMessage());
				++noMatches;
			}
		}
	}
	
	private void saveProcess(FileUploadEntity fileUploadEntity) {
		Set<String> keys = data.keySet();
		for (Iterator<String> iterator = keys.iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();
			
			MailingEntity mailing = data.get(key);
			mailing.setFileUpload(fileUploadEntity);
			
			fileUploadEntity.getMailing().add(mailing);
		}
		
		fileUploadEntity.setProcessingEnd(new Date());
		fileUploadEntity.setStatus("processed");
		this.uploadFileService.save(fileUploadEntity);
	}
}
