package com.desafio.developerchallenge.zip;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.desafio.developerchallenge.exception.UploadFileException;
import com.desafio.developerchallenge.service.impl.BillingFileService;
import com.desafio.developerchallenge.service.impl.MalingFileService;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class FileUtils {

	@Value("${zipfile.upload}")
	private String inboxDir;

	@Value("${zipfile.processing}")
	private String processingDir;

	@Value("${zipfile.backup}")
	private String backupDir;

	private String localPath = new File("").getAbsolutePath();

	/**
	 * @param file
	 * @throws UploadFileException
	 */
	public List<File> unzipFile(MultipartFile file, String receivingProtocol) throws UploadFileException {
		List<File> csvFiles = new ArrayList<File>();
		this.checkDir(this.inboxDir);

		try {
			csvFiles = unzip(file.getInputStream(), receivingProtocol);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return csvFiles;
	}

	public List<File> unzip(InputStream is, String receivingProtocol) {
		List<File> csvFiles = new ArrayList<File>();
		try {
			byte[] buffer = new byte[1024];

			ZipInputStream zipIs = new ZipInputStream(is);
			ZipEntry ze = zipIs.getNextEntry();

			while (ze != null) {
				String fileName = ze.getName();
				File newFile = new File(inboxDir + File.separator + receivingProtocol + "_" + fileName);
				log.info("file unzip : " + newFile.getAbsoluteFile());

				new File(newFile.getParent()).mkdirs();

				FileOutputStream fos = new FileOutputStream(newFile);

				int len;
				while ((len = zipIs.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}

				csvFiles.add(newFile);

				fos.close();
				ze = zipIs.getNextEntry();
			}

			zipIs.closeEntry();
			zipIs.close();

			log.info("Unzip Done !");

		} catch (Exception e) {
			throw new UploadFileException(e.getMessage());
		}
		return csvFiles;
	}

	public void checkDir(String path) {
		File folder = new File(path);
		if (!folder.exists()) {
			folder.mkdir();
		}
	}

	public void moveFilesProcessing(String protocol) {
		this.moveFiles(protocol, inboxDir, processingDir);
	}

	public void moveFilesBackup(String protocol) {
		this.moveFiles(protocol, processingDir, backupDir);
	}

	public void moveFiles(String protocol, String source, String destiny) {
		try {
			this.checkDir(destiny);

			Files.move(
					FileSystems.getDefault()
							.getPath(localPath + File.separator + source + File.separator + protocol + "_"
									+ MalingFileService.FILE_POSFIX),
					FileSystems.getDefault().getPath(localPath + File.separator + destiny + File.separator + protocol
							+ "_" + MalingFileService.FILE_POSFIX));

			Files.move(
					FileSystems.getDefault()
							.getPath(localPath + File.separator + source + File.separator + protocol + "_"
									+ BillingFileService.FILE_POSFIX),
					FileSystems.getDefault().getPath(localPath + File.separator + destiny + File.separator + protocol
							+ "_" + BillingFileService.FILE_POSFIX));

		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
	}

	public File getMailingFile(String protocol) {
		return new File(localPath + File.separator + processingDir + File.separator + protocol + "_"
				+ MalingFileService.FILE_POSFIX);
	}

	public File getBillingFile(String protocol) {
		return new File(localPath + File.separator + processingDir + File.separator + protocol + "_"
				+ BillingFileService.FILE_POSFIX);
	}

	public List<List<String>> readCSV(File file) {
		List<List<String>> records = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line;
			while ((line = br.readLine()) != null) {
				log.info("(readCSV) line: " + line);
				String[] values = line.replaceAll("\"", "").split(";");
				records.add(Arrays.asList(values));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return records;
	}
}
