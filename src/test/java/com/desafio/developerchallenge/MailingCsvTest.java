package com.desafio.developerchallenge;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.desafio.developerchallenge.service.impl.MalingFileService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MailingCsvTest {

	@Autowired
	private MalingFileService service;
	
	@Test(expected = Exception.class)
	public void cpfTestFail() throws Exception {
		
		String csvLine = "\"GABRIEL DA SILVA\";\"RUA AV. PAULISTA 100\";\"57\";\"444.444.444-44\"";
		List<String> csv = new ArrayList<String>(); 
		csv.add(csvLine);
		
		service.load(csv);
	}
	
	@Test(expected = Exception.class)
	public void ageTestFail() throws Exception {
		
		String csvLine = "\"JOSE DA SILVA\";\"RUA AV. PAULISTA 296\";\"4sa\";\"363.219.270-74\"";
		List<String> csv = new ArrayList<String>(); 
		csv.add(csvLine);
		
		service.load(csv);		
	}
}
