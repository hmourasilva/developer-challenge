package com.desafio.developerchallenge;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.desafio.developerchallenge.zip.FileUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ZipTests {
	
	@Autowired
	private FileUtils fileUtils;

	
	@Test
	public void unzipQtdFileTest() throws FileNotFoundException {
		String protocol = UUID.randomUUID().toString();
		String localPath = new File("").getAbsolutePath();
		InputStream  zipFile = new FileInputStream(localPath + File.separator + "zip/input_files.zip");
		List<File> list = this.fileUtils.unzip(zipFile, protocol);
		
		 assertEquals(2, list.size());
		 list.get(0).delete();
		 list.get(1).delete();
	}
	
	
	@Test
	public void unzipMailingFileTest() throws FileNotFoundException {
		String protocol = UUID.randomUUID().toString();
		
		String localPath = new File("").getAbsolutePath();
		InputStream  zipFile = new FileInputStream(localPath + File.separator + "zip/input_files.zip");
		List<File> list = this.fileUtils.unzip(zipFile, protocol);
		
		File mailingFile = fileUtils.getMailingFile(protocol);
		assertEquals(protocol + "_mailing.csv", mailingFile.getName());
		
		list.get(0).delete();
		list.get(1).delete();
	}
}
